import os as OS
import sys as SYS
import numpy as NP
import pandas as PD
# from tqdm import tqdm



# import workflow tools from the other files
SYS.path.append('workflow')
PREP = __import__('01_DataPreparation') # weird way of library import because of the numbered file names
ANG = __import__('02_ExtractJointAngleProfiles') 



# load master data
master_data = PD.read_csv('master_data.csv', sep = ';')
# master_data = master_data.loc[master_data['day'].values == 20210330, :]
# master_data = master_data.loc[master_data['notes'].values == 'new', :]

# master_data = PD.read_csv('tracking/master_data_backup.csv', sep = ';')
# master_data = master_data.loc[[9,19], :]
# print (master_data)

# append a base folder
base_folder = '/data/Jessie/11_wildboars'

for col in ['data_folder', 'dlt_filename']:
    master_data[col] = [OS.sep.join([base_folder, filepath]) for filepath in master_data[col].values]



# work through all the entries
for nr, params in master_data.T.to_dict().items():
	print ('\n', '_'*80)
	for p in ['cycle_start_frame', 'cycle_duration']:
		if NP.isnan(params[p]):
			params[p] = None
	print (params)

    # 
	PREP.ProcessSingleVideo(params)
	ANG.ProcessSingleStride(params)


# cycle_start_frame, cycle_duration = [ 16;334 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec022a
# cycle_start_frame, cycle_duration = [ 82;315 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec022b
# cycle_start_frame, cycle_duration = [ 39;180 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec023
# cycle_start_frame, cycle_duration = [ 3;352 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec033
# cycle_start_frame, cycle_duration = [ 2;205 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec037a
# cycle_start_frame, cycle_duration = [ 4;275 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec037b
# cycle_start_frame, cycle_duration = [ 3;278 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec041
# cycle_start_frame, cycle_duration = [ 4;190 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec043
# cycle_start_frame, cycle_duration = [ 1;160 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec163
# cycle_start_frame, cycle_duration = [ 6;216 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day2_rec170
# cycle_start_frame, cycle_duration = [ 14;296 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec005
# cycle_start_frame, cycle_duration = [ 21;190 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec014
# cycle_start_frame, cycle_duration = [ 7;227 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec015
# cycle_start_frame, cycle_duration = [ 3;277 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec021
# cycle_start_frame, cycle_duration = [ 21;192 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec032
# cycle_start_frame, cycle_duration = [ 24;196 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec047
# cycle_start_frame, cycle_duration = [ 10;248 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec073
# cycle_start_frame, cycle_duration = [ 10;220 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec103
# cycle_start_frame, cycle_duration = [ 9;227 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec133
# cycle_start_frame, cycle_duration = [ 10;193 ] /data/Jessie/11_wildboars/wildboar_analysis/tracking/day3_rec152

# [ 24;192 ] /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_109/tracking
# [ 26;186 ] /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_066/tracking
# [ 3;213 ] /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_096a/tracking
# [ 10;213 ] /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_096b/tracking
# [ 27;183 ] /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_101/tracking
# [ 26;250 ] /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_005a_tinneke/tracking
# [ 24;297 ] /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_005b_rudy/tracking

# cycle_start_frame, cycle_duration = [ 9;234 ] /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_011_tinneke/tracking