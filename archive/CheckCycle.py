import os as OS
import numpy as NP
import pandas as PD
import scipy.ndimage as NDI 

import matplotlib as MP
import matplotlib.pyplot as MPP


cycle_file = 'log_cycles.csv'

raw_cycles = PD.read_csv(cycle_file, sep = ';')

# print (raw_cycles)



# minima = []
# for ref in NP.unique(raw_cycles['ref'].values):
#     sub_data = raw_cycles.loc[raw_cycles['ref'].values == ref, :]
    
#     x = sub_data['dfr'].values
#     y = sub_data['pd'].values
#     MPP.plot(x, y, color = '0.5', alpha = 0.2)

#     miny = NP.argmin(y)
#     minima.append(x[miny])
#     MPP.scatter(x[miny], y[miny], s = 3, marker = 'v', color = '0.1', alpha = 0.2)


# MPP.hist(minima, bins = 64, histtype = 'step', color = '0.7', density = True)


# MPP.gca().set_xlim([NP.min(raw_cycles['dfr'].values), NP.max(raw_cycles['dfr'].values)])

# MPP.show()




### Step 2: find the best matching frame

# prepare storage dict
cycle_info = { 'ref': [], 'cycle': []}

# loop all ref frames
for ref in NP.unique(raw_cycles['ref'].values):

    # NOTE: the following step still assumes that frames were consecutive

    # looking forward
    sub_data = raw_cycles.loc[NP.logical_and((raw_cycles['ref'].values == ref), (raw_cycles['dfr'].values > 0)), ['dfr', 'pd']].set_index('dfr', inplace = False)

    # try to find most similar frame
    if sub_data.shape[0] == 0:
        most_similar_frame = NP.nan
    else:
        try:
            most_similar_frame = sub_data.index.values[NP.nanargmin(sub_data['pd'].values.ravel())]
        except ValueError as ve:
            # this catches the cases where no corresponding frames were found ("all-None argmin")
            most_similar_frame = NP.nan
            continue

    cycle_info['ref'].append(ref)
    cycle_info['cycle'].append(most_similar_frame)

cycle_info = PD.DataFrame.from_dict(cycle_info).set_index('ref', inplace = False)

print (cycle_info)

print (cycle_info.values.ravel())
print (NDI.gaussian_filter1d(cycle_info.values.ravel(), sigma = 3))
print (NP.gradient(NDI.gaussian_filter1d(cycle_info.values.ravel(), sigma = 10) ))


# exclude cycles where incomplete strides entered the comparison above
full_cycles = NP.nanargmin( NP.gradient(NDI.gaussian_filter1d(cycle_info.values.ravel(), sigma = 8) ) > -1. )

print (full_cycles)

# !!! the "sigma" changes it.