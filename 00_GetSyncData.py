import os as OS
import sys as SYS
import numpy as NP
import pandas as PD
import re as RE
from tqdm import tqdm


# load master data
master_data = PD.read_csv('master_data.csv', sep = ';').set_index(['day', 'recording'], inplace = False)
# master_data = PD.read_csv('tracking/master_data_backup.csv', sep = ';').set_index(['day', 'recording'], inplace = False)
master_data = master_data.loc[master_data['notes'].values == 'CHECK', :]
print (master_data)

# append a base folder
base_folder = '/data/Jessie/11_wildboars'

for (day, rec), row in master_data.iterrows():
    data_folder = OS.sep.join([base_folder] + row['data_folder'].split(OS.sep)[:-1])

    sync_file = OS.sep.join([data_folder, '01_gatherframes.py'])

    try:
        with open(sync_file) as fi:
            print ('\n', sync_file)
            start_frame = None
            sync_list = None

            for line in fi:
                if line.startswith('start_frame = '.strip()):
                    search_str = line.strip()
                    found = RE.match(r'start_frame = ([\d+\.]*)', search_str)
                    if found:
                        start_frame = list(map(float, found.groups()))[0]
                    # print ('sf', start_frame)

                if line.startswith('sync_frames_list = '.strip()):
                    search_str = line.strip()
                    found = RE.match(r'sync_frames_list = \[(([\d+\.]*[, ]?)*)\]', search_str)
                    if found:
                        sync_list = found.groups()[0].split(',')
                        sync_list = list(map(lambda fnr: float(fnr.strip()), sync_list))
                        # print ('sl', sync_list)
                        sync_list -= NP.min(sync_list)
                        sync_list += start_frame

                if (not (start_frame is None)) and (not (sync_list is None)):
                    break

        print ((day, rec), data_folder, "|".join(map(lambda txt: f'{txt:.1f}', sync_list)), '\n')



    except FileNotFoundError as fnfe:
        print (day, rec, fnfe)
        continue


# for col in ['data_folder', 'dlt_filename']:
#     master_data[col] = [OS.sep.join([base_folder, filepath]) for filepath in master_data[col].values]


"""
(20200917, '22') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_022 13.0|25.0|0.0|4.0
(20200917, '22') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_022 13.0|25.0|0.0|4.0
(20200917, '22b') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_022 13.0|25.0|0.0|4.0
(20200917, '23') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_023 145.0|178.0|162.0|154.0
(20200917, '33') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_033 207.0|200.0|180.0|196.0
(20200917, '037a') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_037a 5.0|13.0|9.0|1.0
(20200917, '037b') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_037b 304.0|312.0|308.0|300.0
(20200917, '41') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_041 260.0|287.0|275.0|284.0
(20200917, '43') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_043 179.0|184.0|150.0|183.0
(20200917, '163') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_163 470.0|471.0|450.0|471.0
(20200917, '170') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_170 525.0|532.0|540.0|524.0
(20201030, '5') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_005 372.0|396.0|360.0|372.0
(20201030, '14') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_014 2546.0|2567.0|2545.0|2547.0
(20201030, '15') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_015 3000.0|3000.0|2975.0|3002.0
(20201030, '21') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_021 327.0|332.0|325.0|332.0
(20201030, '32') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_032 1850.0|1855.0|1820.0|1835.0
(20201030, '47') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_047 1005.0|1029.0|995.0|1009.0
(20201030, '73') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_073 2433.0|2437.0|2420.0|2432.0
(20201030, '73fm') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_073fm 92.0|95.0|80.0|92.0 
(20201030, '103') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_103 98.0|106.0|75.0|97.0
(20201030, '133') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_133 329.0|337.0|325.0|334.0
(20201030, '152') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_152 1560.0|1568.0|1558.0|1555.0 
(20200917, '109') /data/Jessie/11_wildboars/20200917_everzwijnen2/rec_109 35.0|9.0|16.0|0.0 
(20201030, '66') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_066 9280.0|9306.0|9280.0|9293.0 
(20201030, '96a') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_096a 240.0|242.0|203.0|241.0 
(20201030, '96b') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_096b 180.0|182.0|143.0|181.0 
(20201030, '101') /data/Jessie/11_wildboars/20201030_everzwijnen3/rec_101 833.0|836.0|827.0|825.0 
(20210330, '005a') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_005a_tinneke 6349.0|6341.0|6347.0|6326.0 
(20210330, '005b') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_005b_rudy 6349.0|6341.0|6347.0|6326.0 
(20210330, '11') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_011_tinneke 856.0|844.0|846.0|855.0 
(20210330, '13') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_013_tinneke 800.0|803.0|801.0|784.0 
(20210330, '23') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_023_tinneke 500.0|500.0|486.0|482.0 
(20210330, '24') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_024_tinneke 717.0|719.0|700.0|692.0 
(20210330, '29') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_029_rudy 2901.0|2907.0|2911.0|2912.0 
(20210330, '31') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_031_rudy 2786.0|2786.0|2789.0|2792.0 
(20210330, '44') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_044_rudy 2500.0|2512.0|2506.0|2516.0 
(20210330, '47') /data/Jessie/11_wildboars/20210330_everzwijnen4/rec_047_rudy 2408.0|2419.0|2417.0|2427.0 
"""