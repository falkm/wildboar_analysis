import os as OS
import sys as SYS
import numpy as NP
import pandas as PD
# from tqdm import tqdm

import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



# import workflow tools from the other files
SYS.path.append('workflow')
# weird way of library import because of the numbered file names
PREP = __import__('01_DataPreparation') 
ANG = __import__('02_ExtractJointAngleProfiles') 
FPA = __import__('03_FourierProcrustesAlignment') 




# load master data
master_data = PD.read_csv('master_data.csv', sep = ';')
# master_data = PD.read_csv('tracking/master_data_backup.csv', sep = ';')
# master_data = master_data.loc[[11,12,13], :]
# print (master_data.T)

limbs = FPA.LoadLimbs(master_data)

reference_joint = 'ipsfore'
test_joint = 'confore'


plot_joint = test_joint

fig = MPP.figure()
fig.suptitle(f"{test_joint.replace('_', ' ')}, aligned by {reference_joint.replace('_', ' ')}")

ax = fig.add_subplot(2,1,1)
ax.set_ylabel('pre')
for idx, lmb in limbs.items():
    lmb.Plot(ax, subset_joints = [plot_joint])


# do the alignment AND return averages
avg_ref = FPA.Alignment(limbs, reference_joint = reference_joint, n_iterations = 5 \
            , superimposition_kwargs = dict(skip_shift = True, skip_scale = True, skip_rotation = False) \
            )

# store
for idx, lmb in limbs.items():
    lmb.Save(master_data.loc[idx, 'fourier_storage'].replace('_fsd.csv', f'_{reference_joint}.lmb'))

# contralateral phase
print({idx: lmb[test_joint].GetMainPhase() for idx, lmb in limbs.items()})

# amplitude
print({idx: NP.sum(lmb[reference_joint].GetAmplitudes()) for idx, lmb in limbs.items()})

# continue plotting
avg = FPA.FT.ProcrustesAverage( \
                            [lmb[plot_joint] for lmb in limbs.values()] \
                            , n_iterations = 5, skip_scale = True, post_align = False \
                            )
ax.plot(avg.Reconstruct(x_reco = lmb.time), color = 'k', ls = '-')



ax = fig.add_subplot(2,1,2, sharex = ax, sharey = ax)
ax.plot(avg.Reconstruct(x_reco = lmb.time), color = 'k', ls = '-') # plot avg again
ax.set_ylabel('post')
for idx, lmb in limbs.items():
    lmb.Plot(ax, subset_joints = [plot_joint])

ax.set_xlim([0., 100.])
MPP.legend()
MPP.show()




"""
mcarp PRE for fm-tracked episodes
158  re    im
n            
0  0.37  0.00
1  0.14 -0.12
2  0.01 -0.15
3 -0.07 -0.05
4 -0.06  0.02
5 -0.00  0.02
6 -0.00  0.02

073  re    im
n            
0  0.26  0.00
1 -0.01  0.23
2 -0.16 -0.02
3  0.04 -0.13
4  0.03  0.04
5 -0.03  0.01
6 -0.02 -0.03
"""