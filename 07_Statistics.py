

import numpy as NP # numerical operations
import pandas as PD # data analysis


import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting
import seaborn as SB # plotting (chic)

from scipy.stats import pearsonr


# load data
data = PD.read_csv('results/all_data.csv', sep = ';').set_index('master_id', inplace = False)
print (data)

# load extra info
master_info = PD.read_csv('results/extra_info.csv', sep = ';').set_index('master_id', inplace = False)
print(master_info)


### (2) a plot for categorical variables
# https://seaborn.pydata.org/examples/jitter_stripplot.html
SB.set_theme(style="whitegrid")
# data = SB.load_dataset("iris")
# print (data)

# "Melt" the dataset to "long-form" or "tidy" representation
data_melted = PD.melt(data, [col for col in master_info.columns], var_name="PC")
print (data_melted)

# Initialize the figure
f, ax = MPP.subplots()
SB.despine(bottom=False, left=True)


variable_of_interest = "size_category"
# variable_of_interest = "sex"

# Show each observation with a scatterplot
SB.stripplot(x="value", y="PC", hue=variable_of_interest,
              data=data_melted, dodge=True, alpha=.4, zorder=1)

# Show the conditional means
SB.pointplot(x="value", y="PC", hue=variable_of_interest,
              data=data_melted, dodge=.532, join=False, palette="dark",
              markers="d", scale=.75, ci=None)

# Improve the legend 
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[3:], labels[3:], title=variable_of_interest.replace('_', ' '),
          handletextpad=0, columnspacing=1,
          loc="lower right", ncol=3, frameon=True)

ax.axhline(-0.5, color = 'k', ls = '-', lw = 0.5)
for tick in ax.get_yticks():
    ax.axhline(tick+0.5, color = 'k', ls = '-', lw = 0.5)


MPP.show()

