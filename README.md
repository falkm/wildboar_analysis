# wildboar_analysis

Code for analyzing wild boar kinematic data.
Including: 
- Kinematic Data Processing
- Fourier Series Analysis (cf. https://doi.org/10.1093/zoolinnean/zlz135 )