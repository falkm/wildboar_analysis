################################################################################
### Analysis Procedure                                                       ###
################################################################################
__author__      = "Falk Mielke"
__date__        = 20210324

"""
This functions below will take data from aligned (or unaligned) joint angle 
systems and perform multivariate analysis. 


Falk Mielke
2021/03/24
"""


################################################################################
### Libraries                                                                ###
################################################################################
#_______________________________________________________________________________
import sys as SYS           # system control
import os as OS             # operating system control and file operations
import re as RE             # regular expressions, to extract patterns from text strings
import numpy as NP          # numerical analysis
import pandas as PD         # data management

# load self-made toolboxes
SYS.path.append('../toolboxes') # makes the folder where the toolbox files are located accessible to python
import EigenToolbox as ET


# plotting:
import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



#_______________________________________________________________________________
# helper variables and functions
re_im = ['re', 'im']

## Ordered unique elements of a nested list
# https://stackoverflow.com/a/716489
# https://twitter.com/raymondh/status/944125570534621185
CombineNestedList = lambda original: list(dict.fromkeys(sum(original, [])))


################################################################################
### Procedure                                                                ###
################################################################################

def LimbToDataRow(lmb, all_joints, all_components):
    # get a single data row for one limb

    joint_data = []
    for jnt in all_joints:
        joint_fsd = lmb[jnt][all_components, :].values.ravel(order = 'C')
        joint_data.append(joint_fsd)

    return NP.concatenate(joint_data, axis = 0)


def LimbsToDataMatrix(limbs):
    # convert a dict of limbs to a data matrix

    # get all joints in the data set
    all_joints = CombineNestedList([list(lmb.keys()) for lmb in limbs.values()])
    all_components = CombineNestedList([list(lmb[jnt]._c.index.values) for jnt in all_joints for lmb in limbs.values()])

    # print (all_joints)

    # prepare an empty data frame
    data = PD.DataFrame(index = [key for key in limbs.keys()], columns = [f"{jnt}|{comp}|{ri}" for jnt in all_joints for comp in all_components for ri in re_im])

    # loop limbs
    for key, lmb in limbs.items():
        # get a single data row 
        data_row = LimbToDataRow(lmb, all_joints, all_components)

        # append data
        data.loc[key, :] = data_row

    # remove 0|im columns
    data.drop(columns = [col for col in data.columns if '|0|im' in col], inplace = True)

    # return
    return data


def PCA(data, dim = None):

    data.loc[:, :] = NP.array(data.values, dtype = NP.float64)

    pca = ET.PrincipalComponentAnalysis(data, features = data.columns) # reduce_dim_to = 20

    if dim is not None:
        if dim == 'auto':
            weights = pca.weights
        else:
            weights = pca.weights[:21]

        pca.ReduceDimensionality(redu2dim = dim, threshold = 0.99)
    
    # print (NP.sum(pca.weights), len(pca.weights))

    return pca


inch = 2.54 
cm = 1/inch
def PlotPCA2D(pca, dims = [0,1], labels = None, figax = None, figargs = {'dpi': 300, 'figsize': (16*cm, 16*cm) }, scatterargs = {} ):
    if figax is not None:
        fig, ax = figax
    else:
        fig = MPP.figure(**figargs)
        ax = fig.add_subplot(1, 1, 1, aspect = 'equal')

    values = pca.transformed.iloc[:, dims]
    ax.scatter(values.iloc[:, 0], values.iloc[:, 1], **scatterargs)

    if labels is not None:
        for idx, val in values.iterrows():
            ax.text(  val[0], val[1] \
                    , labels[idx] \
                    , ha = 'left', va = 'bottom' \
                    , fontsize = 8 \
                    , alpha = 0.6 \
                    )


    ax.axhline(0, color = 'k', ls = '-', lw = 0.5)
    ax.axvline(0, color = 'k', ls = '-', lw = 0.5)

    ax.set_xlabel(f'PC{dims[0]+1} ({100*pca.weights[dims[0]]:0.1f} %)')
    ax.set_ylabel(f'PC{dims[1]+1} ({100*pca.weights[dims[1]]:0.1f} %)')

    return [fig, ax]




def PlotPCA(pca, dims = range(3), labels = None ):

    fig = MPP.figure(**{'dpi': 300, 'figsize': (20*cm, 26*cm) })

    gs = MP.gridspec.GridSpec( \
                                  len(dims) \
                                , 1 \
                                , height_ratios = [1]*len(dims) \
                                , width_ratios = [1] \
                                )


    values = pca.transformed.iloc[:, dims]

    ref_ax = None
    for d in dims:
        if ref_ax is None:
            ax = fig.add_subplot(gs[d])
            ref_ax = ax
        else:
            ax = fig.add_subplot(gs[d], sharex = ref_ax)


        for idx, v in values.iloc[:, d].iteritems():
            ax.axvline(v, color = 'k', ls = '-', lw = 1)

            if labels is not None:
                ax.text(  v, 0.05 \
                        , labels[idx] \
                        , ha = 'left', va = 'bottom' \
                        , fontsize = 8 \
                        , alpha = 0.6 \
                        , rotation = 60 \
                        )

        ax.set_ylim([0., 1.])
        ax.set_yticks([])
        ax.set_ylabel(f'PC{d+1} ({100*pca.weights[d]:0.1f}%)', fontsize = 8)

    return fig




################################################################################
### Testing                                                                  ###
################################################################################



################################################################################
### Mission Control                                                          ###
################################################################################
if __name__ == "__main__":
    # here an example of how to process a single video
    pass
