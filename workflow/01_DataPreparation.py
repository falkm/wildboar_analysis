################################################################################
### Kinematics Data Preparation                                              ###
################################################################################
__author__      = "Falk Mielke"
__date__        = 20210114

"""
This script and collection of functions is used to transform data
from video point digitization on multiple camera perspectives. 
This focuses on preparing a single tracked episode. 

Sub-Routines:
- load csv files tracked with the Progressive Tracker 
  (https://gitlab.com/falkm/progressivetracker)
- reconstruct 3D point data
- find stride cycles

Goal: store a single stride cycle 3D data
      this can be "batch-applied" to many data files

The process starts at the main script below (if __name__ == "__main__").


Questions are welcome (falkmielke.biology@mailbox.org)

Falk Mielke
2021/01/14
"""



################################################################################
### Libraries                                                                ###
################################################################################
#_______________________________________________________________________________
import sys as SYS           # system control
import os as OS             # operating system control and file operations
import re as RE             # regular expressions, to extract patterns from text strings
import numpy as NP          # numerical analysis
import scipy.ndimage as NDI # for a smoothing function
import pandas as PD         # data management

# load self-made toolboxes
for tbfolder in ['toolboxes', '../toolboxes']:
    if OS.path.isdir(tbfolder):
        SYS.path.append(tbfolder) # makes the folder where the toolbox files are located accessible to python
import SuperimpositionToolbox as ST # point superimposition tools (e.g. Procrustes)


# plotting:
import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



#_______________________________________________________________________________
# helper variables
xyz = ['x', 'y', 'z'] # 3d coordinate shorthand
xy = ['x', 'y'] # 2d coordinate shorthand


# optionally print progress
verbose = True
StatusText = lambda *txt, **kwargs: print (*txt, **kwargs)

def SetSilent():
    # a function to mute output of this script from another control script
    global StatusText
    StatusText = lambda *txt, **kwargs: None
    verbose = False

    
if not verbose:
    SetSilent()


################################################################################
### Data I/O                                                                 ###
################################################################################
#_______________________________________________________________________________
def LoadSingleEpisodeKinematics(data_folder):
    # This function will load the digitized points from a single recording.
    # CSV files have to be in a folder, one per camera, and no other CSVs.
    # filenames should contain 
    #     - "FM%i" where %i is the 1 digit camera number
    #     - "rec%03.0f" where %03.0f is the 3 digit recording id
    #
    # input:  data_folder -> folder (str) with the tracking results
    # output: info        -> dict with information for each camera
    # output: data        -> pandas DataFrame with the tracked data


    StatusText('_'*50, f'\n ### loading data from {data_folder} ###')

    # load all csv files in the folder; sorted
    files = list(sorted([fi for fi in OS.listdir(data_folder) if OS.path.splitext(fi)[-1] == '.csv']))
    # print (files)

    # extract file info
    info = {} # information about each file
    data = [] # data; will be combined to a big data frame later

    # loop files to get the info
    for filename in files:

        ## (1) camera information from filename
        # info differs per camera number
        camera_nr = RE.search(r"_FM(\d)_", filename).group(1)
        
        # store filename in a nested dictionary
        info[camera_nr] = {'filename': filename}

        # store recording nr
        # print (filename)
        info[camera_nr]['rec_nr'] = RE.search(r"_rec(\d*)", filename).group(1) 
        


        ## (2) data
        # load the raw data per camera
        camerawise_data = PD.read_csv(f"{data_folder}{OS.sep}{filename}", sep = ';')

        # replace "folder" column by camera number
        info[camera_nr]['folders'] = NP.unique(camerawise_data['folder'].values)
        camerawise_data.drop(columns = ['folder'], inplace = True) # delete the folder column
        camerawise_data['camera_nr'] = camera_nr # instead, identify by camera number

        # adjust indexing to easier access to the data
        camerawise_data.set_index(['camera_nr', 'frame_nr'], inplace = True)


        # add this camera to the list of data tables
        data.append(camerawise_data)


    ## combine all data to a big file
    kinematics = PD.concat(data, axis = 0)

    # # print random lines from the data
    # print (kinematics.sample(5))

    # return info and kinematic data
    StatusText('... done!')
    return info, kinematics





################################################################################
### 3D data reconstruction                                                   ###
################################################################################
#_______________________________________________________________________________
def UVtoXYZ(pts, dlt):
    # retrieve Object Points (3D) from multiple perspective Image Points
    # adapted from https://github.com/kilmoretrout/argus_gui/blob/master/argus_gui/tools.py `uv_to_xyz`
    #     pts: (N x 2K) array of N 2D points for K cameras
    #          can be a single point over time, or multiple points in one scene
    #     dlt: (11 x K) array of DLT parameters
    # Adjusted because data points herein are undistorted prior to calculation
    
    # initialiye empty data array
    xyzs = NP.empty((len(pts), 3))
    xyzs[:] = NP.nan
    
    # for each point 
    for i in range(len(pts)):
        uvs = list()
        # for each uv pair
        for j in range(len(pts[i]) // 2):
            # do we have a NaN pair?
            if not True in NP.isnan(pts[i, 2 * j:2 * (j + 1)]):
                # if not append the undistorted point and its camera number to the list
                uvs.append([pts[i, 2 * j:2 * (j + 1)], j])

        if len(uvs) > 1:
            # if we have at least 2 uv coordinates, setup the linear system
            A = NP.zeros((2 * len(uvs), 3))

            # assemble coefficient matrix of the linear system
            for k in range(len(uvs)):
                A[k] = NP.asarray([uvs[k][0][0] * dlt[uvs[k][1]][8] - dlt[uvs[k][1]][0],
                                   uvs[k][0][0] * dlt[uvs[k][1]][9] - dlt[uvs[k][1]][1],
                                   uvs[k][0][0] * dlt[uvs[k][1]][10] - dlt[uvs[k][1]][2]])
                A[k + 1] = NP.asarray([uvs[k][0][1] * dlt[uvs[k][1]][8] - dlt[uvs[k][1]][4],
                                       uvs[k][0][1] * dlt[uvs[k][1]][9] - dlt[uvs[k][1]][5],
                                       uvs[k][0][1] * dlt[uvs[k][1]][10] - dlt[uvs[k][1]][6]])

            # the dependent variables
            B = NP.zeros((2 * len(uvs), 1))
            for k in range(len(uvs)):
                B[k] = dlt[uvs[k][1]][3] - uvs[k][0][0]
                B[k + 1] = dlt[uvs[k][1]][7] - uvs[k][0][1]

            # solve it
            xyz = NP.linalg.lstsq(A, B, rcond=None)[0]
            # place in the proper frame
            xyzs[i] = xyz[:, 0]

    return xyzs


#_______________________________________________________________________________
def LoadCameraCalibration(dlt_filename, cams):
    # Load camera calibration information
    # from a CSV file
    # make sure to select the right calibration file for a particular recording!
    # cf. http://mielke-bio.info/falk/camera_calibration
    #
    # input:  dlt_filename -> file (str) with DLT parameters (from calibration)
    # input:  cams         -> list of camera numbers which should match the indices in the DLT csv file
    # output: dlt          -> camera calibration information (dlt parameters) DataFrame

    # read the csv file
    dlt = PD.read_csv(dlt_filename, sep = ';').set_index('Unnamed: 0', inplace = False)

    # select relevant data block
    dlt = dlt.loc[[f'L_{nr}' for nr in range(11)], [f'FM{cam}' for cam in cams]]
    
    # return DLT parameters
    return dlt



#_______________________________________________________________________________
def DataReconstruction3D(calibration, kinematics_2d):
    # reconstructs 3D landmark trajectories over time (frame_nr) using DLT parameters.
    #
    # input:  calibration    -> camera calibration information (dlt parameters) DataFrame with cam_nr as column
    # input:  kinematics_2d  -> pandas DataFrame with the tracked data; "landmark_xy" in columns
    # output: reconstruction -> reconstructed 3D kinematic data; time in rows, landmarks in columns

    StatusText('_'*50, '\n ### reconstructing 3D data ###')

    # list of camera numbers
    cam_numbers = [RE.search(r"FM(\d)", col).group(1) for col in calibration.columns]

    # check for equal frame numbers: only frame numbers which are in each file
    from functools import reduce # https://numpy.org/devdocs/reference/generated/numpy.intersect1d.html
    frame_numbers = NP.sort(reduce(NP.intersect1d, [kinematics_2d.loc[cam, :].index.values for cam in cam_numbers]))

    # get the landmarks
    landmarks = NP.unique([col[:-2] for col in kinematics_2d.columns])

    # initialize a data list for storing reconstructions per landmark
    reconstruction = []

    # loop landmarks
    for lm in landmarks:
        # get the 2D data per camera, concatenated horizontally, as numpy array
        dataarray = NP.concatenate([kinematics_2d.loc[cam_nr, :].loc[frame_numbers, [f'{lm}_{coord}' for coord in xy]].values for cam_nr in cam_numbers], axis = 1)

        # reconstruct the data for this landmark
        reco_data = PD.DataFrame(UVtoXYZ(dataarray, calibration.loc[:, [f'FM{cam_nr}' for cam_nr in cam_numbers]].values.T), columns = [f'{lm}_{coord}' for coord in xyz])
        
        # append the storage list
        reconstruction.append(reco_data) 
        

    # combine the list of stored data
    reconstruction = PD.concat(reconstruction, axis = 1)

    # index name
    reconstruction.index.name = 'frame_nr'

    # return the 3D reconstruction
    StatusText('... done!')
    return reconstruction



################################################################################
### Stride Cycle Identification                                              ###
################################################################################

def IdentifyStrideCycles(kinematics_3d_raw, landmarks = None):
    # from kinematic data, find which frames most resemble each other
    # resembling means that the point configuration is as similar as possible
    # thereby, stride cycles are identified
    # 
    # input:    kinematics_3d -> pandas DataFrame with the reconstructed 3d data; "landmark_xyz" in columns
    # optional: landmarks     -> list of landmarks to use; if "None", all columns of the data are taken
    # output:   cycles        -> previous/next full cycle for each frame in the data

    StatusText('_'*50, '\n ### automatically finding stride cycles ###')


    ## Settings
    # plausible interval: stride cycles are assumed to be between ... frames long:
    search_interval = [100, 500]# [100, 500]


    ## Preparation
    # perform a deep copy of the input data
    # this allows us to modify the structure of the data without changing the original
    kinematics_3d = kinematics_3d_raw.copy()

    # modify the columns into a MultiIndex, for easier xyz access
    kinematics_3d.columns = PD.MultiIndex.from_tuples([(col[:-2], col[-1]) for col in kinematics_3d.columns])


    # get a list of the available landmarks
    if landmarks is None:
        landmarks = kinematics_3d.columns.levels[0].values

    # get the relevant frames
    all_frames = kinematics_3d.index.values



    ### step 1: compare all with all frames (slow)

    # store the raw_cycles as follows.
    #   ref: reference frame
    #   fr: delta frames
    #   n: number of available landmarks
    #   pd: procrustes distance (similarity measure)
    raw_cycles = {'ref': [], 'dfr': [], 'n': [], 'pd': []}


    ## Looping; skipping the last frames
    for frame_nr, ref_frame in enumerate(kinematics_3d.index.values[:-search_interval[0]]): 
        # go through all relevant "reference frames"
        # and calculate similarity to all "probe frames"

        # print progress
        if (ref_frame % 25) == 0:
            StatusText(f'frame {frame_nr}/{len(all_frames)} ...', end = '\r', flush = True)

        # get the reference frame point positions in nx3 table format; sorting and dropping NaN's
        ref_points = kinematics_3d.loc[ref_frame, :].unstack(level=1).loc[landmarks, xyz].dropna(inplace = False)


        for probe_frame in all_frames[NP.logical_and(all_frames >= ref_frame+search_interval[0], all_frames <= ref_frame+search_interval[1])]:
            # the probe frame is a videoframe which happened after the reference frame.
            # both frames are compared below, quantifying their difference

            # memory note: to also look backward, append
            #   all_frames[NP.logical_and(all_frames >= ref_frame - search_interval[1], all_frames <= ref_frame - search_interval[0])]

            # store which frames are compared
            raw_cycles['ref'].append(ref_frame)
            raw_cycles['dfr'].append(probe_frame - ref_frame)

            # get the probe frame point positions in nx3 table format; sorting and dropping NaN's
            probe_points = kinematics_3d.loc[probe_frame, :].unstack(level=1).loc[landmarks, xyz].dropna(inplace = False)

            # only use points which are tracked in reference and probe frame
            common_points = ref_points.join(probe_points, how = 'inner', lsuffix = '_ref', rsuffix = '_probe')

            # count the number of available points
            # print(common_points.shape[0], len(landmarks))
            raw_cycles['n'].append( common_points.shape[0]/len(landmarks) )
            
            # at least 6 points are required, otherwise cyclization might be erratic
            if common_points.shape[0] < 6:
                raw_cycles['pd'].append(NP.nan) 
                continue # proceed the loop if too few points are tracked in both frames

            # if a sufficient number of points are found in both frames,
            # extract the common points
            ref_form = common_points.loc[:, [f'{coord}_ref' for coord in xyz]].values.astype(float)
            probe_form = common_points.loc[:, [f'{coord}_probe' for coord in xyz]].values.astype(float)

            # perform Porcrustes Superimposition
            # this will attempt to "shift/scale/rotate" data from the two frames onto each other
            # and return "procrustes distance" (pd) a quantity that measures how different the time points are.
            # cf. http://mielke-bio.info/falk/procrustes
            _, pd, _ = ST.Procrustes( ref_form, probe_form )

            # store the procrustes distance, normalized by the number of points
            raw_cycles['pd'].append(pd / common_points.shape[0])


    StatusText(f'frame {len(all_frames)}/{len(all_frames)} done!')

    # create a data frame
    raw_cycles = PD.DataFrame.from_dict(raw_cycles)

    # # debugging 20210202
    # raw_cycles.to_csv('log_cycles.csv', sep = ';', index = False)



    ### Step 2: find the best matching frame

    # prepare storage dict
    cycle_info = { 'ref': [], 'cycle': []}

    # loop all ref frames
    for ref in NP.unique(raw_cycles['ref'].values):

        # NOTE: the following step still assumes that frames were consecutive

        # looking forward
        sub_data = raw_cycles.loc[NP.logical_and((raw_cycles['ref'].values == ref), (raw_cycles['dfr'].values > 0)), ['dfr', 'pd']].set_index('dfr', inplace = False)

        # try to find most similar frame
        if sub_data.shape[0] == 0:
            most_similar_frame = NP.nan
        else:
            try:
                most_similar_frame = sub_data.index.values[NP.nanargmin(sub_data['pd'].values.ravel())]
            except ValueError as ve:
                # this catches the cases where no corresponding frames were found ("all-None argmin")
                most_similar_frame = NP.nan
                continue

        cycle_info['ref'].append(ref)
        cycle_info['cycle'].append(most_similar_frame)

    cycle_info = PD.DataFrame.from_dict(cycle_info).set_index('ref', inplace = False)


    # exclude cycles where incomplete strides entered the comparison above
    # this works because when the cycle is complete, it is unlikely that successive hits are shorter by one frame.
    full_cycles = NP.argmin( NP.gradient(NDI.gaussian_filter1d(cycle_info.values.ravel(), sigma = 8) ) > -1. )

    # # check plot
    # MPP.plot(cycle_info.index.values, NP.gradient(NDI.gaussian_filter1d(cycle_info.values.ravel(), sigma = 3) ))
    # MPP.axhline(-1)
    # MPP.axvline(cycle_info.index.values[full_cycles])
    # MPP.show()
    # print (full_cycles)
    # # consider adjusting sigma! 

    # only get full cycles
    cycle_info = cycle_info.iloc[:full_cycles, :]

    # return the cycle info
    StatusText('... done!')
    return cycle_info


#_______________________________________________________________________________
def AutoStrideCycleExtraction(kinematics_3d, cycle_start_frame, cycle_duration, cycle_storage):
    # Automatic stride cycle extraction
    # heuristics to get a good start, and a good cycle length
    # unless those are manually specified


    # find cyclic data
    cycles = IdentifyStrideCycles(kinematics_3d)

    # store cycle info
    cycles.to_csv(cycle_storage, sep = ';')

    # get a good start, centered in the tracked data
    if cycle_start_frame is None:
        cycle_start_frame = cycles.index.values[cycles.shape[0] // 2]

    # retrieve the according cycle length
    if cycle_duration is None:
        cycle_duration = cycles.loc[cycle_start_frame, 'cycle']


    return cycle_start_frame, cycle_duration




################################################################################
### Example and Testing                                                      ###
################################################################################
def ProcessSingleVideo(params):
    ## main porcedure
    # params must be a dict; required fields: 
    #   - data_folder # folder with the tracking files
    #   - dlt_filename # link to the camera calibration
    #   - cycle_start_frame # optional: start of the stride cycle
    #   - cycle_duration # optional: length of the stride cycle
    #   - cycle_storage # where to store the output

    # load camera info and data
    info, kinematics = LoadSingleEpisodeKinematics(params['data_folder'])

    # load spatial calibration
    calibration = LoadCameraCalibration(params['dlt_filename'], cams = [cam_nr for cam_nr in info.keys()])

    # reconstruct 3D data
    kinematics_3d = DataReconstruction3D(calibration, kinematics)


    ## Automatic stride cycle extraction
    # you can run this once by setting "None" values and then enter the outcomes above
    if (params['cycle_start_frame'] is None) or (params['cycle_duration'] is None):
        params['cycle_start_frame'], params['cycle_duration'] = AutoStrideCycleExtraction(kinematics_3d, params['cycle_start_frame'], params['cycle_duration'], params['cycle_storage'])
        print (f"cycle_start_frame, cycle_duration = [ {params['cycle_start_frame']};{params['cycle_duration']} ]", params['data_folder'])


    # store a single cycle
    kinematics_3d = kinematics_3d.loc[params['cycle_start_frame']:params['cycle_start_frame']+params['cycle_duration']]
    kinematics_3d.to_csv(params['kinematics_storage'], sep = ';')

    ### plot an overview of the landmark shifts 
    # to identify discontinuities in landmarks
    fig = MPP.figure(dpi = 300)
    ax = fig.add_subplot(1,1,1)

    landmarks = NP.unique([col[:-2] for col in kinematics_3d.columns])
    t = kinematics_3d.index.values[:-1]
    for lm in landmarks:
        values = kinematics_3d.loc[:, [f'{lm}_{coord}' for coord in xyz]].values
        diffs = NP.sum(NP.abs(NP.diff(values, axis = 0)), axis = 1)
        ax.plot(t, diffs, lw = 1., alpha = 1., label = lm.replace('_', ' '))

    ax.legend(ncol = 2, fontsize = 8)
    ax.set_title(OS.path.split(params['kinematics_storage'])[-1].replace('.csv', '').replace('_kinematics', ''))
    MPP.gca().set_xlim([t[0],t[-1]])
    MPP.tight_layout()
    MPP.savefig(params['kinematics_storage'].replace('.csv', '.png'), dpi = 300, transparent = False)
    MPP.close()



    return kinematics_3d


def ExampleRoutine():
    # use test data to demonstrate the usage of the functions above 

    ## user input
    input_parameters = {'000': {}} # all parameters for one cycle can be stored in a dictionary. This is nested in a dictionary of all recordings. 
    input_parameters['000']['data_folder'] = '../test_data/tracking' # where the tracking data is
    input_parameters['000']['dlt_filename'] = '../test_data/calibration/20200917_cal6_parameters.csv' # where spatial calibration is found

    input_parameters['000']['cycle_start_frame'], input_parameters['000']['cycle_duration'] = [5, 189] # [None, None] # where stride cycle starts, and how many frames long it is

    input_parameters['000']['cycle_storage'] = '../test_data/kinematics_3d/test_cycles.csv' # where to store the cycle comparison
    input_parameters['000']['kinematics_storage'] = '../test_data/kinematics_3d/teststride.csv' # where to store the stride data


    # by using the parameters sub-dictionary below, you can easily process multiple recordings in a loop as follows:
    for rec_nr, parameters in input_parameters.items():
        # extract a stride cycle
        kinematics_3d = ProcessSingleVideo(parameters)

        # plot coordinates over time
        MPP.plot(kinematics_3d)
        MPP.show()


################################################################################
### Mission Control                                                          ###
################################################################################
if __name__ == "__main__":
    # here an example of how to process a single video
    ExampleRoutine()
        
