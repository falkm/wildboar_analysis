################################################################################
### Fourier Procrustes Alignment                                             ###
################################################################################
__author__      = "Falk Mielke"
__date__        = 20210312

"""
This script will load the fourier coefficients of the tracked episodes and 
align them by the reference angle (normally ipsilateral forelimb).


Falk Mielke
2021/03/12
"""


################################################################################
### Libraries                                                                ###
################################################################################
#_______________________________________________________________________________
import sys as SYS           # system control
import os as OS             # operating system control and file operations
import re as RE             # regular expressions, to extract patterns from text strings
import numpy as NP          # numerical analysis
import pandas as PD         # data management
# import scipy.ndimage as NDI # for a smoothing function
# import scipy.interpolate as INTP # interpolation and smoothing
# import scipy.spatial.distance as DIST # cross distance (difference) of data values

# load self-made toolboxes
for tbfolder in ['toolboxes', '../toolboxes']:
    if OS.path.isdir(tbfolder):
        SYS.path.append(tbfolder) # makes the folder where the toolbox files are located accessible to python
# import QuaternionGeometryToolbox as QGT # point superimposition tools (e.g. Procrustes)
import FourierToolbox as FT


# plotting:
import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



#_______________________________________________________________________________
# helper variables



################################################################################
### Procedure                                                                ###
################################################################################
def LoadLimbs(master_data):
    limbs = {}
    for idx, info in master_data.iterrows():
        coefficient_df = PD.read_csv(info['fourier_storage'], sep = ';').set_index(['re_im', 'coeff'], inplace = False)
        # coefficient_df = coefficient_df.loc[:, [reference_joint, test_joint]]

        # print (coefficient_df)

        lmb = FT.NewLimb(coefficient_df, label = idx, coupled = True)

        # lmb.PrepareRelativeAlignment(test_joint, skip_rotate = True, skip_center = False, skip_normalize = False)

        limbs[idx] = lmb

    return limbs


def ReLoadLimbs(master_data, reference_joint):

    limbs = {}
    for idx, row in master_data.iterrows():
        limb_file = row['fourier_storage'].replace('_fsd.csv', f'_{reference_joint}.lmb')
        limbs[idx] = FT.NewLimb.Load(limb_file)

    return limbs


def Alignment(some_limbs, reference_joint = 'ipsfore', n_iterations = 5, superimposition_kwargs = dict(skip_shift = False, skip_scale = False, skip_rotation = False)):


    average = FT.ProcrustesAverage( \
                            [lmb[reference_joint] for lmb in some_limbs.values()] \
                            , n_iterations = n_iterations, skip_scale = True, post_align = False \
                            )




    for lmb in some_limbs.values():
        # # standardize the shift and amplitude of the focal joint to give relative values
        # lmb.PrepareRelativeAlignment(focal_joint, skip_rotate = True, skip_center = skip_precenter, skip_normalize = skip_prenorm)
        
        # align all joints, based on the reference
        lmb.AlignToReference(reference_joint, average, superimposition_kwargs = superimposition_kwargs)

    return average



################################################################################
### Visualization                                                            ###
################################################################################
def CheckPlotJoints(limbs):
    fig = MPP.figure()
    ax = fig.add_subplot(1,1,1)

    for idx, lmb in limbs.items():
        lmb.Plot(ax)

    MPP.show()



################################################################################
### Testing                                                                  ###
################################################################################

def ExampleAlignment(master_data):

    reference_joint = 'ipsfore'
    test_joint = 'ipsfore'
    
    limbs = LoadLimbs(master_data)

    # print (limbs['000'][test_joint])
    # CheckPlotJoints(limbs)

    fig = MPP.figure()
    ax = fig.add_subplot(2,1,1)
    for idx, lmb in limbs.items():
        lmb.Plot(ax, subset_joints = [reference_joint, test_joint])


    Alignment(limbs, reference_joint = reference_joint)

    # print (limbs['000'][test_joint])
    # CheckPlotJoints(limbs)
    ax = fig.add_subplot(2,1,2)
    for idx, lmb in limbs.items():
        lmb.Plot(ax, subset_joints = [reference_joint, test_joint])
    MPP.show()


def ExampleLimb(label, info):

    coefficient_df = PD.read_csv(info['fourier_storage'], sep = ';').set_index(['re_im', 'coeff'], inplace = False)

    limb = FT.NewLimb(coefficient_df, label = label, coupled = True)
    # print (limb)
    return limb


def ExampleRoutine():
    # use test data to demonstrate the usage of the functions above 

    ## user input
    input_parameters = {'000': {}, '001': {}} # all parameters for one cycle can be stored in a dictionary. This is nested in a dictionary of all recordings. 
    input_parameters['000']['fourier_storage'] = '../test_data/kinematics_3d/testfouriercoefficients_1.csv' # where to LOAD the Fourier data
    input_parameters['001']['fourier_storage'] = '../test_data/kinematics_3d/testfouriercoefficients_2.csv' # where to LOAD the Fourier data
    
    # label = '000'
    # ExampleLimb(label, input_parameters[label])
    # ExampleLimb('001', input_parameters['001'])

    
    # testlimb = ExampleLimb('001', input_parameters['001'])
    # print (testlimb.Copy().ToDict())
    # testlimb.Save('test.lmb')

    # loadlimb = FT.Limb.Load('test.lmb')

    # print (loadlimb.ToDict())
    # print (loadlimb['head'])


    input_parameters = PD.DataFrame.from_dict(input_parameters).T
    input_parameters.index.name = 'rec_id'

    ExampleAlignment(input_parameters)



################################################################################
### Mission Control                                                          ###
################################################################################
if __name__ == "__main__":
    # here an example of how to process a single video
    ExampleRoutine()
