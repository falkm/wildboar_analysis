################################################################################
### Kinematics Data Preparation                                              ###
################################################################################
__author__      = "Falk Mielke"
__date__        = 20210115

"""
This script and collection of functions is used to extract joint kinematics
from previously prepared stride cycle data.

Sub-Routines:
- load stride cycle csv files prepared with 01_DataPreparation
- calculate joint angle profiles
- convert cyclic angle profiles to frequency domain via Fourier Series

Goal: load stride cycle and extract relevant measures
      this can be "batch-applied" to many data files

The process starts at the main script below (if __name__ == "__main__").


Questions are welcome (falkmielke.biology@mailbox.org)

Falk Mielke
2021/01/15
"""


################################################################################
### Libraries                                                                ###
################################################################################
#_______________________________________________________________________________
import sys as SYS           # system control
import os as OS             # operating system control and file operations
import re as RE             # regular expressions, to extract patterns from text strings
import numpy as NP          # numerical analysis
import scipy.ndimage as NDI # for a smoothing function
import scipy.interpolate as INTP # interpolation and smoothing
import scipy.spatial.distance as DIST # cross distance (difference) of data values
import pandas as PD         # data management

# load self-made toolboxes
for tbfolder in ['toolboxes', '../toolboxes']:
    if OS.path.isdir(tbfolder):
        SYS.path.append(tbfolder) # makes the folder where the toolbox files are located accessible to python
import QuaternionGeometryToolbox as QGT # point superimposition tools (e.g. Procrustes)
import FourierToolbox as FT


# plotting:
import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



#_______________________________________________________________________________
# helper variables
xyz = ['x', 'y', 'z'] # 3d coordinate shorthand
reim = ['re', 'im'] # real/imaginary coordinate shorthand


# optionally print progress
verbose = True
StatusText = lambda *txt, **kwargs: print (*txt, **kwargs)

def SetSilent():
    # a function to mute output of this script from another control script
    global StatusText
    StatusText = lambda *txt, **kwargs: None
    verbose = False


if not verbose:
    SetSilent()



################################################################################
### Structural Settings                                                      ###
################################################################################
# these are setting which should rarely change within the scope of this project.

# joint definitions
# a dictionary relating joint names to quadruplets of points
# the first two points form the proximal segment vector, the latter two the distal one
# second and third points may be the same
# always use proximal to distal order
joint_definitions = { \
                      'head': ['snout', 'eye', 'withers', 'croup'] \
                    , 'withers': ['croup', 'withers', 'withers', 'if_shoulder'] \
                    , 'shoulder': ['croup', 'withers', 'if_shoulder', 'if_elbow'] \
                    , 'elbow': ['if_shoulder', 'if_elbow', 'if_elbow', 'if_mcarp'] \
                    , 'mcarp': ['if_elbow', 'if_mcarp', 'if_mcarp', 'if_wrist'] \
                    , 'wrist': ['if_mcarp', 'if_wrist', 'if_wrist', 'if_hoof'] \
                    , 'ipsfore': ['croup', 'withers', 'withers', 'if_hoof'] \
                    , 'confore': ['croup', 'withers', 'withers', 'cf_hoof'] \
                    }
# Note: For now, the shoulder angle is taken relative to the backline 
#       because the withers/shoulder connection might be less well determined (hidden scapula).




################################################################################
### Joint Angle Calculation                                                  ###
################################################################################
def JointAngleCalculation(kinematics_3d_raw, side_invert = False):
    # Calculate Joint Angles over time for the joints defined above

    # perform a deep copy of the input data
    # this allows us to modify the structure of the data without changing the original
    kinematics_3d = kinematics_3d_raw.copy()

    # modify the columns into a MultiIndex, for easier xyz access
    kinematics_3d.columns = PD.MultiIndex.from_tuples([(col[:-2], col[-1]) for col in kinematics_3d.columns])


    # prepare output data frame
    joint_angles = PD.DataFrame(index = kinematics_3d.index.copy())

    # loop through all joints and calculate angles
    for joint, landmarks in joint_definitions.items():

        # get positions of each landmark
        positions = [kinematics_3d.loc[:, lm].loc[:, xyz].values for lm in landmarks]

        # calculate vectors = differences between two points, pointing distally
        proximal_vector = positions[0] - positions[1]
        distal_vector = positions[3] - positions[2]

        # calculate joint angle quaternion
        joint_ang = [QGT.FindQuaternionRotation(q_prox, q_dist).angle() \
                    for q_prox, q_dist \
                    in zip(QGT.QuaternionFrom3D(proximal_vector), QGT.QuaternionFrom3D(distal_vector)) \
                    ]
        # MPP.plot(NP.concatenate([joint_q, [NP.nan], joint_q]), label = joint)

        # if side_invert:
        #     joint_ang = list(NP.pi - NP.array(joint_ang))

        # store the joint angle
        joint_angles[joint] = joint_ang

    
    # MPP.legend(loc = 'best')
    # MPP.show()

    # return the raw joint angles
    return joint_angles




################################################################################
### Fourier Coefficient Affine Superimposition                               ###
################################################################################
### Fourier smoothing
def SmoothTrace(angle, order = 16, plot = False):
    # fourier smoothing of the trace
    time = NP.linspace(0., 3., 3*len(angle), endpoint = False)
    
    signal = NP.concatenate([angle]*3)
    
    fsd = FT.FourierSignal.FromSignal(time, signal, order = order, period = 3.)
    smooth_signal = fsd.Reconstruct(x_reco = time, period = 3.)
    
    if plot:
        MPP.plot(time, signal)
        MPP.plot(time, smooth_signal)
        MPP.gca().axvline(time[len(angle)])
        MPP.gca().axvline(time[2*len(angle)])
        MPP.show();
    
    return smooth_signal[len(angle):2*len(angle)]

#___________________________________________________________________________________________________
### start/end correction
def MakeCyclical(angle):
    # because there can still be a small difference from end to start, angle traces are forced to be cyclical.

    # first, a bit of gentle smoothing
    smoothed_trace = SmoothTrace(angle, order = 16)
    
    # take a few frames before and after the end
    frame = 8

    # get end-start difference
    endstart_difference = NP.mean(smoothed_trace[-frame:]) - NP.mean(smoothed_trace[:frame])
    
    #... and spread correction of the difference linearly over the whole stride cycle
    return NP.subtract(angle, NP.linspace(0, endstart_difference, len(angle), endpoint = True))
    

#_______________________________________________________________________________
def CyclizeProfiles(joint_angles):
    # make joint profiles cyclical
    # by spreading the end-start difference over the stride cycle

    # store the "time" dimension (required for smoothing)
    time = joint_angles.index.values


    # loop angles
    for col in joint_angles.columns:

        # take one raw angular profile
        angle = joint_angles.loc[:, col].values.ravel()

        # correct the column
        joint_angles.loc[:, col] = MakeCyclical(angle)

    # # check plot:
    #     MPP.plot(NP.concatenate([joint_angles[col].values, [NP.nan], joint_angles[col].values]), label = col)

    # MPP.legend()
    # MPP.show()



#_______________________________________________________________________________
# calculate Fourier coefficients
def CalculateFourierCoefficients(joint_angles, order = 3, plotfile = None):
    # converts the joint angles into the frequency domain

    # required below: all relevant fourier coefficients for each joint angle
    indices = [(coeff, ri) for coeff in range(order+1) for ri in reim if not (f'{coeff}_{ri}' == '0_im')]
    
    # prepare a storage data frame
    fourier_coefficients = PD.DataFrame(index = PD.MultiIndex.from_tuples(indices))
    fourier_coefficients.index.names = ['coeff', 're_im']


    # common time
    time = NP.linspace(0., 1., len( joint_angles.index.values ))

    # loop all joint angles
    for jnt, angle in joint_angles.T.iterrows():

        # fourier series decomposition
        fsd = FT.FourierSignal.FromSignal(time = time, signal = angle.values, order = order, label = jnt)

        # get coefficients
        coefficients = fsd.GetCoeffDataFrame()
        
        # if jnt == 'mcarp':
        #     print(coefficients)

        # append storage data frame
        fourier_coefficients[jnt] = NP.array([coefficients.loc[coeff, ri] for coeff, ri in indices])

        if plotfile is not None:
            MPP.plot(time, angle.values, color = 'k', lw = 1, ls = '--', alpha = 0.6, label = None)
            MPP.plot(time, fsd.Reconstruct(time), lw = 1., alpha = 1., label = jnt)

    if plotfile is not None:
        MPP.legend(ncol = 2)
        MPP.gca().set_xlim([0.,1.])
        MPP.savefig(plotfile, dpi = 300, transparent = False)


        MPP.close()

    return fourier_coefficients



################################################################################
### Example and Testing                                                      ###
################################################################################
def ProcessSingleStride(params):
    ## main porcedure
    # params must be a dict; required fields: 
    #   - cycle_storage # csv file to load the input stride cycle
    #   - angle_storage # csv file to store the joint angle profiles
    #   - fourier_storage # csv file to store the Fourier coefficients

    # load kinematics
    kinematics_3d = PD.read_csv(params['kinematics_storage'], sep = ';').set_index('frame_nr', inplace = False)
    

    ## Joint Angles
    # limb side
    # if PD.isnull(params['side_towards']):
        # params['side'] = 'R'

    # perform joint angle calculation
    joint_angles = JointAngleCalculation(kinematics_3d, side_invert = params.get('sync_frames_list', 'R').upper() == 'L')

    # store joint angles
    joint_angles.to_csv(params['angle_storage'], sep = ';')


    ## data cosmetics    
    CyclizeProfiles(joint_angles) # cycles are corrected in the data frame ("call-by-reference")


    ## Fourier Series
    fourier_coefficients = CalculateFourierCoefficients(joint_angles, order = params['fsd_order'], plotfile = params['fourier_storage'].replace('.csv', '.pdf'))

    # store the results
    fourier_coefficients.to_csv(params['fourier_storage'], sep = ';')




def ExampleRoutine():
    # use test data to demonstrate the usage of the functions above 

    ## user input
    input_parameters = {'000': {}} # all parameters for one cycle can be stored in a dictionary. This is nested in a dictionary of all recordings. 
    input_parameters['000']['kinematics_storage'] = '../test_data/kinematics_3d/teststride.csv' # where to LOAD the stride kinematics
    input_parameters['000']['angle_storage'] = '../test_data/kinematics_3d/testjointangles.csv' # where to LOAD the angles over time
    input_parameters['000']['fsd_order'] = 6 # FSD order 
    input_parameters['000']['fourier_storage'] = '../test_data/kinematics_3d/testfouriercoefficients.csv' # where to store the Fourier data
    input_parameters['000']['side'] = 'l'
    # make sure to also adjust the joint triplets if necessary

    # by using the keyword argument notation (i.e. function(**kwargs)") below, you can easily process multiple recordings in a loop as follows:
    for rec_nr, parameters in input_parameters.items():
        # extract a stride cycle
        kinematics_3d = ProcessSingleStride(parameters)



################################################################################
### Mission Control                                                          ###
################################################################################
if __name__ == "__main__":
    # here an example of how to process a single video
    ExampleRoutine()