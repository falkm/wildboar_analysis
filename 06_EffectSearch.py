

import numpy as NP # numerical operations
import pandas as PD # data analysis


import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting
import seaborn as SB # plotting (chic)

from scipy.stats import pearsonr


# load master info
master_info = PD.read_csv('results/master_info.csv', sep = ';').set_index('master_id', inplace = False)


# filter out "excluded" recordings
master_data = PD.read_csv('master_data.csv', sep = ';')
master_data = master_data.loc[NP.logical_not(master_data['excluded'].values), :]
master_data.index.name = 'master_id'

master_info = master_info.loc[master_data.index.values, :]



# # exclude two
# master_info = master_info.loc[[idx for idx in master_info.index.values if not (idx in [15, 16])], :]



# load PCA result
pca_values = PD.read_csv('results/pca.csv', sep = ';').set_index('master_id', inplace = False)

## additional info
extra_info = {}
# from "03_FourierAlign"
extra_info['confore_phase'] = {0: 0.5766673322876237, 1: 0.5857237281125199, 2: 0.5783480181184735, 3: 0.560385439596821, 4: 0.5113761360683199, 5: 0.562351697191475, 6: 0.531268818802924, 7: 0.5591242773333469, 8: 0.5491756852104995, 9: 0.5309647107589219, 10: 0.5472269356633765, 11: 0.606781768003366, 12: 0.5862960262914984, 13: 0.5704161277698587, 14: 0.5578441953273678, 15: 0.5571497169999239, 16: 0.6313250431296522, 17: 0.5469751472904116, 18: 0.5957572221244865, 19: 0.5656405702783671, 20: 0.5377008000774741, 21: 0.5411212201315598, 22: 0.5514311649241167, 23: 0.552895430944923, 24: 0.5758812526491813, 25: 0.5515171590551906, 26: 0.6033265814028216, 27: 0.5594501337964279, 28: 0.5656207799144755, 29: 0.5728754145351616, 30: 0.5487750781229925, 31: 0.5487925313094653, 32: 0.5743917408343586, 33: 0.5847579085856283, 34: 0.5429347653151049, 35: 0.5517305239962523, 36: 0.5531115487545845, 37: 0.5389668072536132, 38: 0.5697616975993531}
extra_info['swing_amplitude'] = {0: 0.5861131416840423, 1: 0.5504548580568469, 2: 0.5767496605754445, 3: 0.5389549632710541, 4: 0.4704203975936433, 5: 0.5028043910509141, 6: 0.5351539203018779, 7: 0.562360540963089, 8: 0.4259874264841331, 9: 0.5827745556398225, 10: 0.5929909359450347, 11: 0.3557383486564314, 12: 0.5189852618731525, 13: 0.5871504450038233, 14: 0.6221982081347704, 15: 0.5014997421759854, 16: 0.5344809999173893, 17: 0.6022974036192998, 18: 0.5849795926802173, 19: 0.6029608263520828, 20: 0.5373545517899858, 21: 0.5855808945212242, 22: 0.5682759996951156, 23: 0.49643344201112866, 24: 0.41259236828403295, 25: 0.520590042711069, 26: 0.5921263826581429, 27: 0.5688833789614347, 28: 0.6246969415869014, 29: 0.5146291119935121, 30: 0.4799967048494301, 31: 0.5181677643140692, 32: 0.4501345334109286, 33: 0.5328052794219936, 34: 0.5041450957860236, 35: 0.5442418235104874, 36: 0.5627955395564783, 37: 0.5511620764785197, 38: 0.5585751187092104}

# from "05_ExtractAdditionalParameters"
extra_info['bodysize'] = {0: 0.5079805051872995, 1: 0.4143369067436813, 2: 0.3906954018502788, 3: 0.41167722090784276, 4: 0.43894106130830024, 5: 0.4171937246474649, 6: 0.49629397201332315, 7: 0.47724413445629604, 8: 0.4538602310872201, 9: 0.22793953614798415, 10: 0.4271856636403444, 11: 0.40781975419657435, 12: 0.2748587766847766, 13: 0.38258155700593593, 14: 0.5825700308093402, 15: 0.5878902840142212, 16: 0.5648636757452449, 17: 0.5454381193637416, 18: 0.5747869352625989, 19: 0.4987295600446493, 20: 0.4434280046685758, 21: 0.5564129096719167, 22: 0.5411661762815616, 23: 0.5583177085736788, 24: 0.5432876187435498, 25: 0.3310493419750339, 26: 0.33582197690566856, 27: 0.2939640338326784, 28: 0.4094394177181423, 29: 0.5275537341908019, 30: 0.4590866756428991, 31: 0.5301335451120897, 32: 0.5303729778487644, 33: 0.51663763663124, 34: 0.5160352939922892, 35: 0.47214274786367005, 36: 0.4709405815156354, 37: 0.47596461978314575, 38: 0.4744558393396906}
extra_info['meanspeed'] = {0: 0.6448267021272792, 1: 0.6478919482265412, 2: 0.7573903878104995, 3: 0.9721063280212465, 4: 0.49497240603003845, 5: 0.820611253605707, 6: 0.7477262433727867, 7: 0.6593602989013083, 8: 0.9128756533642234, 9: 0.5998301511702457, 10: 0.944527083063227, 11: 0.6574222796636151, 12: 0.7638004639688398, 13: 0.7980278909951624, 14: 0.7000859512708577, 15: 1.030919616438663, 16: 0.7407708048383802, 17: 0.7113181229125127, 18: 0.8807840501627094, 19: 0.9501368237550216, 20: 0.7918680145828443, 21: 0.814938663756651, 22: 0.8197854288803447, 23: 0.8568239603181936, 24: 0.9968102615543942, 25: 0.7508224493962385, 26: 0.5836829530415392, 27: 0.6513564087349635, 28: 0.7234539026293694, 29: 0.4898480302065187, 30: 0.4699313290191862, 31: 0.5278564112093683, 32: 0.3971641623697814, 33: 0.6225867254121181, 34: 0.6294009840852519, 35: 0.5356298973897154, 36: 0.5366654014790516, 37: 0.4035105332617445, 38: 0.41545433793578523}

extra_info['froude'] = {k: extra_info['meanspeed'][k]/extra_info['bodysize'][k] for k in extra_info['bodysize'].keys()}



for param, value_dict in extra_info.items():
    master_info[param] = [value_dict[idx] for idx in master_info.index.values]

# store extra info
master_info.to_csv('results/extra_info.csv', sep = ';')


# join data
n_dims = 8 # only thake first eight PCs
data = master_info.join(pca_values.iloc[:, :n_dims], how = 'left') 

data.to_csv('results/all_data.csv', sep = ';')
print (data)



#### seaborn plots ####

### (1) a correlation plot

# https://stackoverflow.com/questions/50832204/show-correlation-values-in-pairplot-using-seaborn-in-python/50835066
def corrfunc(x, y, ax=None, **kws):
    """Plot the correlation coefficient in the top left hand corner of a plot."""
    r, p = pearsonr(x, y)
    ax = ax or MPP.gca()
    ax.annotate(f'r = {r:.2f}, p = {p:.2e}', xy=(.1, .9), xycoords=ax.transAxes)



g = SB.pairplot(data, y_vars = ['cycle_duration', 'confore_phase', 'swing_amplitude', 'bodysize', 'meanspeed', 'froude'], x_vars = pca_values.columns[:n_dims])
g.map(corrfunc)

MPP.show()

# pause

### (2) a plot for categorical variables
# https://seaborn.pydata.org/examples/jitter_stripplot.html
SB.set_theme(style="whitegrid")
# data = SB.load_dataset("iris")
# print (data)

# "Melt" the dataset to "long-form" or "tidy" representation
data_melted = PD.melt(data, [col for col in master_info.columns], var_name="PC")
print (data_melted)

# Initialize the figure
f, ax = MPP.subplots()
SB.despine(bottom=False, left=True)


# variable_of_interest = "size_category"
variable_of_interest = "type"

# Show each observation with a scatterplot
SB.stripplot(x="value", y="PC", hue=variable_of_interest,
              data=data_melted, dodge=True, alpha=.4, zorder=1)

# Show the conditional means
SB.pointplot(x="value", y="PC", hue=variable_of_interest,
              data=data_melted, dodge=.532, join=False, palette="dark",
              markers="d", scale=.75, ci=None)

# Improve the legend 
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[3:], labels[3:], title=variable_of_interest.replace('_', ' '),
          handletextpad=0, columnspacing=1,
          loc="lower right", ncol=3, frameon=True)

ax.axhline(-0.5, color = 'k', ls = '-', lw = 0.5)
for tick in ax.get_yticks():
    ax.axhline(tick+0.5, color = 'k', ls = '-', lw = 0.5)

MPP.show()




