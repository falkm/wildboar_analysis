import os as OS
import sys as SYS
import numpy as NP
import pandas as PD
from tqdm import tqdm


import matplotlib as MP
import matplotlib.pyplot as MPP


fps = 240


# load master data
master_data = PD.read_csv('master_data.csv', sep = ';').set_index(['day', 'recording'], inplace = False)
# master_data = master_data.loc[master_data['notes'].values == 'CHECK', :]
# master_data = PD.read_csv('tracking/master_data_backup.csv', sep = ';').set_index(['day', 'recording'], inplace = False)


# master_data = master_data.iloc[[5,6,15,16], :]
# print (master_data.T)

# pause

# append a base folder
base_folder = '/data/Jessie/11_wildboars'

index_shift = 0


# iterate recordings
for nr, ((day, rec), row) in enumerate(master_data.iterrows()):
    print ('\n', '#'*20, '\n', day, rec)

    # # testing
    # if (day, rec) in [(20200917, '22'), (20200917, '23'), (20200917, '33')]:
    #     continue

    try:

        # different visuals for frames in the extracted cycle
        if PD.isnull(row['cycle_start_frame']):
            cycle_start_frame = 0
            cycle_duration = 0
        else:
            cycle_start_frame = row['cycle_start_frame']
            cycle_duration = row['cycle_duration']


        # assemble tracking files
        try:
            trackfile_folder = OS.sep.join([base_folder] + row['data_folder'].split(OS.sep))
            tracking_data_files = list(sorted([fi for fi in OS.listdir(trackfile_folder) if fi[-4:] == '.csv']))
            tracking_data_files = list(map(lambda fi: OS.sep.join([trackfile_folder, fi]), tracking_data_files))
            # print (tracking_data_files)
        except FileNotFoundError as fnfe:
            # skip if files not found
            print (day, rec, fnfe)
            continue

        # print (tracking_data_files)
        # continue

        # get the data folder
        data_folder = OS.sep.join([base_folder] + row['data_folder'].split(OS.sep)[:-2])
        
        # check if sync data is present
        if PD.isnull(row['sync']):
            continue

        # get sync data from master file
        start_frames = list(map(float, row['sync'].split('|')))


        # mate a meaningful rec number string
        try:
            rec_nr = f'{int(rec):03.0f}'
        except ValueError:
            rec_nr = rec

        # loop cams
        for cam in range(4):
            # if not (cam == 3):
            #     # testing
            #     continue

            # number offset
            cam_nr = cam+1

            # get tracking data
            tracked_data = PD.read_csv(tracking_data_files[cam], sep = ';') \
                            .drop(columns = 'folder', inplace = False) \
                            .set_index('frame_nr', inplace = False)
            tracked_data.columns = PD.MultiIndex.from_tuples([(col[:-2], col[-1:]) for col in tracked_data.columns])
            # print (tracked_data)




            # remove previous data from frame folders
            OS.system('rm frames_in/* && rm frames_out/* ')

            # get the video file
            video_file = f"{data_folder}/FM{cam_nr}/{day}_rec{rec_nr[:3]}_FM{cam_nr}_{row['type']}.mp4"

            # ... and check if it exists
            if not OS.path.isfile(video_file):
                print ('no file', video_file)
                continue

            # extract frames from that video
            print (f'extracting frames from {video_file}...')
            shift = start_frames[cam]
            frame_count = tracked_data.shape[0]
            command = f'ffmpeg -y -ss {shift/fps} -i {video_file} -t {frame_count/fps} -loglevel panic "frames_in/%04d.png"'
            print ('\t', command)
            OS.system(command)


            # loop through the frames
            for frame_idx, points in tqdm(tracked_data.iterrows()):
                frame_nr = frame_idx+1

                # read the image
                try:
                    img = MPP.imread(f'frames_in/{frame_nr:04.0f}.png')
                except FileNotFoundError as fnfe:
                    # skip if files not found
                    continue


                # prepare a figure
                dpi = 300
                fig = MPP.figure(dpi = dpi, figsize = (1280/dpi, 720/dpi))
                fig.subplots_adjust(left = 0., top = 1., right = 1., bottom = 0.)
                ax = fig.add_subplot(1,1,1, aspect = 'equal')

                # plot image
                ax.imshow(img[:, :, ::-1], origin = 'upper')

                # scatter tracked points
                scatters = NP.stack([points[[(lm, coord) for lm in points.index.levels[0]]].values for coord in ['x', 'y']], axis = 1)
                in_cycle = (frame_idx >= cycle_start_frame) and (frame_idx <= (cycle_start_frame + cycle_duration))
                # print (frame_idx, cycle_start_frame, cycle_duration, in_cycle)
                MPP.scatter(scatters[:, 0], scatters[:, 1], s = 8, marker = 'x', color = 'g' if in_cycle else 'y', lw = 0.5, alpha = 1.0 if in_cycle else 0.6)


                # plot cosmetics
                ax.spines['bottom'].set_visible(False)
                ax.spines['left'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.spines['right'].set_visible(False)


                # save first and last frame of cycle
                if (frame_idx == cycle_start_frame) \
                    or (frame_idx == (cycle_start_frame + cycle_duration)) \
                    :
                    # or ((frame_idx >= (cycle_start_frame - 20)) and (frame_idx <= (cycle_start_frame + 20))) \
                    # or ((frame_idx >= (cycle_start_frame + cycle_duration - 20)) and (frame_idx <= (cycle_start_frame + cycle_duration + 20))) \
                    fig.savefig(f'tracking/check_videos/{nr+index_shift:02.0f}_d{day}_r{rec_nr}_c{cam_nr}_f{frame_nr:04.0f}.png', dpi = dpi, transparent = False)

                # else:
                #     continue # testing
                
                # save and close this frame
                fig.savefig(f'frames_out/{frame_nr:04.0f}.png', dpi = dpi, transparent = False)
                MPP.close()

            # continue # testing
            # combine all frames to an output video
            video_out = f'tracking/check_videos/{nr+index_shift:02.0f}_d{day}_r{rec_nr}_c{cam_nr}.mov'
            print (f'creating video {video_out}...')
            command = f'ffmpeg -y -r 24 -pattern_type glob -i "frames_out/*.png" -c:v mjpeg -q:v 2 -loglevel panic "{video_out}"'
            print ('\t', command)
            OS.system(command)

            print ('done!\n')

    except:
        continue
        
        #     # testing:
        #     break
        # # testing:
        # break


