import os as OS
import sys as SYS
import numpy as NP
import pandas as PD
# from tqdm import tqdm

import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



# import workflow tools from the other files
SYS.path.append('workflow')
# weird way of library import because of the numbered file names
# PREP = __import__('01_DataPreparation') 
# ANG = __import__('02_ExtractJointAngleProfiles') 
FPA = __import__('03_FourierProcrustesAlignment') 
ANP = __import__('04_AnalysisProcedure')



### Load master data
master_data = PD.read_csv('master_data.csv', sep = ';')
master_data = master_data.loc[NP.logical_not(master_data['excluded'].values), :]
master_data.index.name = 'master_id'

print (master_data.iloc[:,:2])

# format correction for recording
master_data['recording'] = [f"{int(rec):03.0f}" if len(rec) < 3 else rec for rec in master_data['recording'].values]

# print (master_data)
# master_data = PD.read_csv('tracking/master_data_backup.csv', sep = ';')

# master_data = master_data.loc[[9,19], :]
# print (master_data.T)


### Load Limbs
reference_joint = 'ipsfore'
limbs = FPA.ReLoadLimbs(master_data, reference_joint = reference_joint)

data = ANP.LimbsToDataMatrix(limbs)


### only get relevant joints
selected_joints = [ \
                      'withers' \
                    , 'shoulder' \
                    , 'elbow' \
                    , 'mcarp' \
                    , 'wrist' \
                  ]

# take only joint columns
data = data.loc[:, [col for col in data.columns if col.split('|')[0] in selected_joints]]

### store data
# print (data)
data.to_csv('results/data_pca_input.csv', sep = ';')


selected_indices = master_data.index.values
# selected_indices = master_data.loc[master_data['type'] == 'wb', :].index.values

pca_selection = NP.array([(idx in selected_indices) for idx in data.index.values], dtype = bool)
pca_data = data.loc[pca_selection, :]
print (pca_data)


### perform the PCA
pca = ANP.PCA(pca_data, dim = 20)
print (pca)


### store results
# store_transformed = pca.transformed # this would store only the selected row subset

store_transformed = pca.TraFo(data.loc[:, :].copy())
store_transformed.index.name = 'master_id'

# store master info
master_data.loc[:, ['day', 'recording', 'excluded', 'cycle_duration', 'sex', 'size_category', 'side_towards', 'type']].to_csv('results/master_info.csv', sep = ';')

# store the PCA result
store_transformed.to_csv('results/pca.csv', sep = ';')


### Get Joint Loadings
# which joint contributes how much to which PC?

loadings = NP.stack(pca.GetLoadings(), axis = 0).T
loadings = PD.DataFrame(loadings, index = pca_data.columns, columns = store_transformed.columns)

# split row index
loadings.index = PD.MultiIndex.from_tuples([tuple(idx.split('|')) for idx in loadings.index])
loadings.index.names = ['joint', 'fsd_coefficient', 'reim']

pc_joint_loadings = loadings.groupby(level = 0).agg(lambda x: NP.sum(NP.abs(x)))

print (pc_joint_loadings)
pc_joint_loadings.to_csv('results/pc_joint_loadings.csv', sep = ';')


### Plotting!
day_to_day = {20200917: 2, 20201030: 3}
labels = {idx: f"d{day_to_day.get(row['day'], 0)}r{row['recording']}" for idx, row in master_data.iterrows()}



# # (A) the common, but misleading 2D PCA plot
# _ = ANP.PlotPCA2D(pca, dims = [0,1] \
#                 , labels = labels \
#                 , scatterargs = dict(s = 20, marker = 'o', edgecolor = 'k', facecolor = 'none', ls = '-', lw = 1) \
#                 )

# MPP.show()


# (B) a linewise plot of the PC components
fig = ANP.PlotPCA(  pca \
            , dims = range(8) \
            , labels = labels \
            )

fig.savefig('results/pca.pdf', transparent = False, dpi = 300)
MPP.show()


