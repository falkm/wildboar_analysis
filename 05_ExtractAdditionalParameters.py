import os as OS
import sys as SYS
import numpy as NP
import pandas as PD
# from tqdm import tqdm

import matplotlib as MP # plotting
import matplotlib.pyplot as MPP # plotting



# # import workflow tools from the other files
# SYS.path.append('workflow')
# # weird way of library import because of the numbered file names
# PREP = __import__('01_DataPreparation') 
# ANG = __import__('02_ExtractJointAngleProfiles') 
# FPA = __import__('03_FourierProcrustesAlignment') 

xyz = ['x', 'y', 'z']
fps = 240

# load master data
master_data = PD.read_csv('master_data.csv', sep = ';')
# master_data = PD.read_csv('tracking/master_data_backup.csv', sep = ';')

# master_data = master_data.loc[[15, 16, 17], :]



def GetPositionParameters(data):

    p1 = data.loc[:, [f'withers_{coordinate}' for coordinate in xyz]].values
    p2 = data.loc[:, [f'croup_{coordinate}' for coordinate in xyz]].values

    # MPP.plot(p1)
    # MPP.plot(p2)
    # MPP.show()

    bodysize_proxy = NP.nanmean(NP.sqrt(NP.sum(NP.power(NP.subtract(p2, p1), 2), axis = 1)))

    meanpt = NP.add(p1, p2)/2.
    dt = NP.nanmean(NP.diff(data.index.values) / fps)
    # print (NP.sum(NP.median(NP.abs(NP.diff(meanpt, axis = 0)), axis = 0)))
    meanspeed = NP.nanmean(NP.sqrt(NP.sum(NP.power(NP.diff(meanpt, axis = 0), 2), axis = 1)))/dt

    meanpt_nonnan = NP.logical_not(NP.any(NP.isnan(meanpt), axis = 1))
    t = data.index.values[meanpt_nonnan]
    meanpt = meanpt[meanpt_nonnan, :]
    diffspeed = NP.sqrt(NP.sum(NP.power(meanpt[-1] - meanpt[0], 2), axis = 0)) / ((t[-1]-t[0])/fps)


    return bodysize_proxy, meanspeed, diffspeed


bodysize = {}
meanspeed = {}
for idx, row in master_data.iterrows():
    # print (row)
    stride_data = PD.read_csv(row['kinematics_storage'], sep = ';')

    bodysize[idx], _, meanspeed[idx] = GetPositionParameters(stride_data)
    print (idx, bodysize[idx], meanspeed[idx])

print (bodysize)
print (meanspeed)
